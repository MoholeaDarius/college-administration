package com.company.repository;

import com.company.model.Student;

import java.io.IOException;
import java.util.List;

public interface StudentRepository {

    List<Student> getAllStudents()throws IOException;

    void addStudent(Student newStudent)throws IOException;

    void removeStudent(int id)throws IOException;

    Student getStudentById(int id)throws IOException;

}
