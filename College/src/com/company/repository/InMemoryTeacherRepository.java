package com.company.repository;

import com.company.model.Teacher;

import java.util.ArrayList;
import java.util.List;

public class InMemoryTeacherRepository implements TeacherRepository {

    private List<Teacher> teachers = new ArrayList<>() {{
        add(new Teacher(1, "Bob", "Raul", 26, "Informatica"));
        add(new Teacher(2, "Galea", "Sorin", 42, "Matematica"));
    }};

    public List<Teacher> getAllTeachers() {
        return teachers;
    }

    public void addTeacher(Teacher newTeacher) {
        newTeacher.setId(createId());
        teachers.add(newTeacher);
    }

    public int createId() {
        return teachers.get(teachers.size() - 1).getId() + 1;
    }

    public void removeTeacher(int id) {
        teachers.remove(getTeacherById(id));
    }

    public Teacher getTeacherById(int id) {
        for (Teacher teacher : teachers) {
            if (teacher.getId() == id) {
                return teacher;
            }
        }
        return null;
    }


}
