package com.company.repository;

import com.company.model.Group;

import java.util.ArrayList;
import java.util.List;

public class InMemoryGroupRepository implements GroupRepository {

    private List<Group> groups = new ArrayList<>() {{
        add(new Group(1, "Informatica"));
        add(new Group(2, "Matematica"));
    }};

    public List<Group> getAllGroups() {
        return groups;
    }

    public void addGroup(Group newGroup) {
        newGroup.setId(createId());
        groups.add(newGroup);
    }

    public int createId() {
        return groups.get(groups.size()-1).getId() + 1;
    }

    public void removeGroup(int id) {
        groups.remove(getGroupById(id));
    }

    public Group getGroupById(int id) {
        for (Group group : groups) {
            if (group.getId() == id) {
                return group;
            }
        }
        return null;
    }

}
