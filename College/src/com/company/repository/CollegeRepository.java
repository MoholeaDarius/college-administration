package com.company.repository;

import com.company.model.College;
import com.company.model.Director;
import com.company.model.Location;

import java.io.IOException;

public interface CollegeRepository {

    College getCollege() throws IOException;

    void changeDirector(Director newDirector) throws IOException;

    void changeLocation(Location newLocation) throws IOException;

    void changeName(String newName) throws IOException;

}
