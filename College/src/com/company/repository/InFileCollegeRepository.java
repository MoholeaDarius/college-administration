package com.company.repository;

import com.company.model.College;
import com.company.model.Director;
import com.company.model.Location;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

import static com.company.repository.Constants.SEPARATOR1;
import static com.company.repository.Constants.SEPARATOR2;

public class InFileCollegeRepository implements CollegeRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "College.txt");

    @Override
    public College getCollege() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.isEmpty()) {
            return null;
        }
        String[] locationArray =  lines.get(2).split(SEPARATOR1);
        Location location = new Location(locationArray[0], Integer.parseInt(locationArray[1].trim()));

        String[] directorArray = lines.get(1).split(SEPARATOR1);
        Director director = new Director(directorArray[0], directorArray[1], Integer.parseInt(directorArray[2].trim()));

        String[] nameArray = lines.get(0).split(SEPARATOR1);
        String name = nameArray[0];

        College college = new College(name, location, director);
        return college;
    }

    @Override
    public void changeDirector(Director newDirector) throws IOException {
        College college = getCollege();
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        college.setDirector(newDirector);
        Files.write(PATH, (FileReady.college(college)+"\n").getBytes(),StandardOpenOption.APPEND);

    }

    @Override
    public void changeLocation(Location newLocation) throws IOException {
        College college = getCollege();
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        college.setLocation(newLocation);
        Files.write(PATH, (FileReady.college(college)+"\n").getBytes(),StandardOpenOption.APPEND);

    }

    @Override
    public void changeName(String newName) throws IOException {
        College college = getCollege();
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        college.setName(newName);
        Files.write(PATH, (FileReady.college(college)+"\n").getBytes(),StandardOpenOption.APPEND);

    }

}
