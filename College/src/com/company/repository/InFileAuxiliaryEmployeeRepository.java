package com.company.repository;

import com.company.model.AuxiliaryEmployee;

import java.awt.image.ImagingOpException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.Constants.SEPARATOR1;

public class InFileAuxiliaryEmployeeRepository implements AuxiliaryEmployeeRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "AuxiliaryEmployees.txt");

    public List<AuxiliaryEmployee> getAuxiliaryEmployees() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.isEmpty()) {
            return new ArrayList<>();
        }
        List<AuxiliaryEmployee> auxiliaryEmployees = new ArrayList<>();
        for (String line : lines) {
            String[] auxiliaryEmployeeArray = line.split(SEPARATOR1);
            AuxiliaryEmployee newAuxiliaryEmployee = new AuxiliaryEmployee(Integer.parseInt(auxiliaryEmployeeArray[0]), auxiliaryEmployeeArray[1], auxiliaryEmployeeArray[2], Integer.parseInt(auxiliaryEmployeeArray[3]), auxiliaryEmployeeArray[4]);
            auxiliaryEmployees.add(newAuxiliaryEmployee);
        }
        return auxiliaryEmployees;
    }

    @Override
    public void addAuxiliaryEmployee(AuxiliaryEmployee newEmployee) throws IOException {
        setAuxiliaryEmployeeId(newEmployee);
        Files.write(PATH,(FileReady.auxiliaryEmployee(newEmployee) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void addAuxiliaryEmployeeWhitId(AuxiliaryEmployee newAuxiliaryEmployee) throws IOException{
        Files.write(PATH,(FileReady.auxiliaryEmployee(newAuxiliaryEmployee) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void setAuxiliaryEmployeeId(AuxiliaryEmployee auxiliaryEmployee) throws IOException {
        List<AuxiliaryEmployee> auxiliaryEmployees = getAuxiliaryEmployees();
        if (auxiliaryEmployees.isEmpty()) {
            auxiliaryEmployee.setId(1);
        } else {
            if (auxiliaryEmployee.getId() == 0) {
                int newAuxiliaryEmployeeId = auxiliaryEmployees.get(auxiliaryEmployees.size() - 1).getId() + 1;
                auxiliaryEmployee.setId(newAuxiliaryEmployeeId);
            }
        }
    }

    @Override
    public void removeAuxiliaryEmployee(int id) throws IOException {
        AuxiliaryEmployee auxiliaryEmployee = getAuxiliaryEmployeeFromId(id);
        if (auxiliaryEmployee==null){
            return;
        }
        List<AuxiliaryEmployee> auxiliaryEmployees = getAuxiliaryEmployees();
        auxiliaryEmployees.remove(auxiliaryEmployee);
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        for (AuxiliaryEmployee auxiliaryEmployee1:auxiliaryEmployees){
            addAuxiliaryEmployeeWhitId(auxiliaryEmployee1);
        }

    }

    @Override
    public AuxiliaryEmployee getAuxiliaryEmployeeFromId(int id) throws IOException {
        AuxiliaryEmployee auxiliaryEmployee = null;
        for (AuxiliaryEmployee auxiliaryEmployee1:getAuxiliaryEmployees()){
            if (auxiliaryEmployee1.getId() == id){
                auxiliaryEmployee = auxiliaryEmployee1;
            }
        }
        return auxiliaryEmployee;
    }


}
