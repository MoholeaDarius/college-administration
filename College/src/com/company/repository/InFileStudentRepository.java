package com.company.repository;

import com.company.model.Student;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.Constants.SEPARATOR1;

public class InFileStudentRepository implements StudentRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "Students.txt");

    public List<Student> getAllStudents() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.size() == 0) {
            return new ArrayList<>();
        }
        List<Student> students = new ArrayList<>();
        for (String line : lines) {
            String[] studentArray = line.split(SEPARATOR1);
            Student newStudent = new Student(Integer.parseInt(studentArray[0]), studentArray[1], studentArray[2], Integer.parseInt(studentArray[3]), Integer.parseInt(studentArray[4]));
            students.add(newStudent);
        }
        return students;
    }

    public void addStudent(Student student) throws IOException {
        setStudentId(student);
        Files.write(PATH, (FileReady.student(student) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    public void addStudentWhitId(Student student) throws IOException {
        Files.write(PATH, (FileReady.student(student) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void setStudentId(Student student) throws IOException {
        List<Student> students = getAllStudents();
        if (students.isEmpty()) {
            student.setId(1);
        } else {
            if (student.getId() == 0) {
                int studentId = students.get(students.size() - 1).getId() + 1;
                student.setId(studentId);
            }
        }
    }

    public void removeStudent(int id) throws IOException {
        Student student = getStudentById(id);
        if (student == null) {
            return;
        }
        List<Student> studentList = getAllStudents();
        studentList.remove(student);
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        for (Student student1 : studentList) {
            addStudentWhitId(student1);
        }
    }


    public Student getStudentById(int id) throws IOException {
        Student student = null;
        for (Student s : getAllStudents()) {
            if (s.getId() == id) {
                student = s;
            }
        }
        return student;
    }


}
