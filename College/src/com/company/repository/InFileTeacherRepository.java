package com.company.repository;

import com.company.model.Student;
import com.company.model.Teacher;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ListIterator;

import static com.company.repository.Constants.SEPARATOR1;

public class InFileTeacherRepository implements TeacherRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "Teachers.txt");

    public List<Teacher> getAllTeachers() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.size() == 0) {
            return new ArrayList<>();
        }
        List<Teacher> teachers = new ArrayList<>();
        for (String line : lines) {
            String[] teacherArray = line.split(SEPARATOR1);
            Teacher teacher = new Teacher(Integer.parseInt(teacherArray[0]), teacherArray[1], teacherArray[2], Integer.parseInt(teacherArray[3]), teacherArray[4]);
            teachers.add(teacher);
        }
        return teachers;
    }

    public void addTeacher(Teacher teacher) throws IOException {
        setTeacherId(teacher);
        Files.write(PATH, (FileReady.teacher(teacher) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void addTeacherWhitId(Teacher teacher) throws IOException {
        Files.write(PATH, (FileReady.teacher(teacher) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    @Override
    public void removeTeacher(int id) throws IOException {
        Teacher teacher = getTeacherById(id);
        if (teacher == null){
            return;
        }
        List<Teacher> teacherList = getAllTeachers();
        teacherList.remove(teacher);
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        for (Teacher teacher1 : teacherList) {
            addTeacherWhitId(teacher1);
        }
    }

    @Override
    public Teacher getTeacherById(int id) throws IOException {
        Teacher teacher = null;
        for (Teacher teacher1 : getAllTeachers()) {
            if (teacher1.getId() == id) {
                teacher = teacher1;
            }
        }
        return teacher;
    }

    public void setTeacherId(Teacher teacher) throws IOException {
        List<Teacher> teachers = getAllTeachers();
        if (teachers.isEmpty()) {
            teacher.setId(1);
        } else {
            if (teacher.getId() == 0) {
                int teacherId = teachers.get(teachers.size() - 1).getId() + 1;
                teacher.setId(teacherId);
            }
        }
    }


}
