package com.company.repository;

import com.company.model.AuxiliaryEmployee;

import java.util.ArrayList;
import java.util.List;

public class InMemoryAuxiliaryEmployeeRepository implements AuxiliaryEmployeeRepository {

    private List<AuxiliaryEmployee> auxiliaryEmployees = new ArrayList<>() {{
        add(new AuxiliaryEmployee(1, "Dansatoarea", "Floriciica", 40, "curatenie"));
        add(new AuxiliaryEmployee(2, "Moldovan", "Maria", 46, "curatenie"));
        add(new AuxiliaryEmployee(3, "Bob", "Abel", 40, "mentenanta"));
        add(new AuxiliaryEmployee(4, "Moldovan", "Silviu", 30, "administrator"));
    }};


    @Override
    public List<AuxiliaryEmployee> getAuxiliaryEmployees() {
        return auxiliaryEmployees;
    }

    @Override
    public void addAuxiliaryEmployee(AuxiliaryEmployee newEmployee) {
        newEmployee.setId(createId());
        auxiliaryEmployees.add(newEmployee);
    }

    public int createId() {
        return auxiliaryEmployees.get(auxiliaryEmployees.size() - 1).getId() + 1;
    }

    @Override
    public void removeAuxiliaryEmployee(int id) {
        auxiliaryEmployees.remove(getAuxiliaryEmployeeFromId(id));
    }

    @Override
    public AuxiliaryEmployee getAuxiliaryEmployeeFromId(int id) {
        for (AuxiliaryEmployee employee : auxiliaryEmployees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }

}
