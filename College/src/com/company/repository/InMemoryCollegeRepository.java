package com.company.repository;

import com.company.model.*;

import java.util.ArrayList;
import java.util.List;

public class InMemoryCollegeRepository implements CollegeRepository {

    private String name = "Ubb";
    private Director director = new Director("Floarea", "Felicia", 39);
    private Location location = new Location("Mehedinti", 23);
    private College college = new College(name, location, director);

    public College getCollege() {
        return college;
    }

    public void changeDirector(Director newDirector) {
        college.setDirector(newDirector);
    }

    public void changeLocation(Location newLocation) {
        college.setLocation(newLocation);
    }

    public void changeName(String newName) {
        college.setName(newName);
    }

}
