package com.company.repository;

import com.company.model.Classroom;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.Constants.SEPARATOR1;

public class InFileClassroomRepository implements ClassroomRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "Classrooms.txt");


    @Override
    public List<Classroom> getAllClassrooms() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.isEmpty()) {
            return new ArrayList<>();
        }
        List<Classroom> classrooms = new ArrayList<>();
        for (String line : lines) {
            String[] classroomArray = line.split(SEPARATOR1);
            Classroom newClassroom = new Classroom(Integer.parseInt(classroomArray[0]), classroomArray[1], Integer.parseInt(classroomArray[2]));
            classrooms.add(newClassroom);
        }
        return classrooms;
    }

    @Override
    public void addClassroom(Classroom newClassroom) throws IOException {
        setClassroomId(newClassroom);
        Files.write(PATH, (FileReady.classroom(newClassroom) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    public void addClassroomWhitId(Classroom newClassroom) throws IOException {
        Files.write(PATH, (FileReady.classroom(newClassroom) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void setClassroomId(Classroom classroom) throws IOException {
        List<Classroom> classrooms = getAllClassrooms();
        if (classrooms.isEmpty()) {
            classroom.setId(1);
        } else {
            if (classroom.getId() == 0) {
                int makeClassroomId = classrooms.get(classrooms.size() - 1).getId() + 1;
                classroom.setId(makeClassroomId);
            }
        }
        return;
    }

    @Override
    public void removeClassroom(int id) throws IOException {
        Classroom classroom = getClassroomFromId(id);
        if (classroom==null){
            return;
        }
        List<Classroom> classrooms = getAllClassrooms();
        classrooms.remove(classroom);
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        for (Classroom classroom1:classrooms){
            addClassroomWhitId(classroom1);
        }
    }

    private Classroom getClassroomFromId(int id) throws IOException {
        Classroom classroom = null;
        for (Classroom classroom1:getAllClassrooms()){
            if (classroom1.getId() == id){
                classroom = classroom1;
            }
        }
        return classroom;
    }

    @Override
    public Classroom getClassroomById(int id) throws IOException {
        return null;
    }
}
