package com.company.repository;

import com.company.model.Group;

import java.io.IOException;
import java.util.List;

public interface GroupRepository {

    List<Group> getAllGroups()throws IOException;

    void addGroup(Group newGroup)throws IOException;

    void removeGroup(int id)throws IOException;

    Group getGroupById(int id)throws IOException;

}
