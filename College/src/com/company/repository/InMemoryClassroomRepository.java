package com.company.repository;

import com.company.model.Classroom;

import java.util.ArrayList;
import java.util.List;

public class InMemoryClassroomRepository implements ClassroomRepository {

    private List<Classroom> classrooms = new ArrayList<>() {{
        add(new Classroom(1, "Informatica", 1));
        add(new Classroom(2, "Matematica", 2));
    }};

    public List<Classroom> getAllClassrooms() {
        return classrooms;
    }

    public void addClassroom(Classroom newClassroom) {
        newClassroom.setId(createId());
        classrooms.add(newClassroom);
    }

    public int createId() {
        return classrooms.get(classrooms.size() - 1).getId() + 1;
    }

    public void removeClassroom(int id) {
        classrooms.remove(getClassroomById(id));

    }

    public Classroom getClassroomById(int id) {
        for (Classroom classroom : classrooms) {
            if (classroom.getId() == id) {
                return classroom;
            }
        }
        return null;
    }

}
