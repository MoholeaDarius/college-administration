package com.company.repository;

import com.company.model.AuxiliaryEmployee;

import java.io.IOException;
import java.util.List;

public interface AuxiliaryEmployeeRepository {

    List<AuxiliaryEmployee> getAuxiliaryEmployees() throws IOException;

    void addAuxiliaryEmployee(AuxiliaryEmployee newEmployee) throws IOException;

    void removeAuxiliaryEmployee(int id) throws IOException;

    AuxiliaryEmployee getAuxiliaryEmployeeFromId(int id) throws IOException;

}
