package com.company.repository;

import com.company.model.Student;

import java.util.ArrayList;
import java.util.List;

public class InMemoryStudentRepository implements StudentRepository {

    private List<Student> students = new ArrayList<>() {{
        add(new Student(1, "Moholea", "Darius", 18, 1));
        add(new Student(2, "Gadalean", "Emanuel", 20, 1));
        add(new Student(3, "Crisan", "Flaviu", 20, 2));
        add(new Student(4, "Zirbo", "Fineas", 21, 2));
    }};

    public List<Student> getAllStudents() {
        return students;
    }

    public void addStudent(Student newStudent) {
        newStudent.setId(createId());
        students.add(newStudent);
    }

    public int createId() {
        return students.get(students.size() - 1).getId() + 1;
    }

    public void removeStudent(int id) {
        students.remove(getStudentById(id));
    }

    public Student getStudentById(int id) {
        for (Student student : students) {
            if (student.getId() == id) {
                return student;
            }
        }
        return null;
    }

}
