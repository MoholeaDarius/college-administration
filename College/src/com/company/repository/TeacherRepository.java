package com.company.repository;

import com.company.model.Teacher;

import java.io.IOException;
import java.util.List;

public interface TeacherRepository {

    List<Teacher> getAllTeachers()throws IOException;

    void addTeacher(Teacher newTeacher)throws IOException;

    void removeTeacher(int id)throws IOException;

    Teacher getTeacherById(int id)throws IOException;

}
