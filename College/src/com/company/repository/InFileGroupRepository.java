package com.company.repository;

import com.company.model.Group;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import static com.company.repository.Constants.SEPARATOR1;

public class InFileGroupRepository implements GroupRepository {

    private static final Path PATH = Paths.get("D:\\College repo 07.19.2021", "Groups.txt");

    @Override
    public List<Group> getAllGroups() throws IOException {
        List<String> lines = Files.readAllLines(PATH);
        if (lines.isEmpty()) {
            return new ArrayList<>();
        }
        List<Group> groups = new ArrayList<>();
        for (String line : lines) {
            String[] groupArray = line.split(SEPARATOR1);
            Group newGroup = new Group(Integer.parseInt(groupArray[0]), groupArray[1]);
            groups.add(newGroup);
        }
        return groups;
    }

    @Override
    public void addGroup(Group newGroup) throws IOException {
        setGroupId(newGroup);
        Files.write(PATH, (FileReady.group(newGroup) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    public void addGroupwhitId(Group newGroup) throws IOException {
        Files.write(PATH, (FileReady.group(newGroup) + "\n").getBytes(), StandardOpenOption.APPEND);
    }

    private void setGroupId(Group group) throws IOException {
        List<Group> groups = getAllGroups();
        if (groups.isEmpty()) {
            group.setId(1);
        } else {
            if (group.getId() == 0) {
                int makeGroupID = groups.get(groups.size() - 1).getId() + 1;
                group.setId(makeGroupID);
            }
        }
        return;
    }

    @Override
    public void removeGroup(int id) throws IOException {
        Group group = getGroupById(id);
        if(group == null){
            return;
        }
        List<Group> groups = getAllGroups();
        groups.remove(group);
        Files.deleteIfExists(PATH);
        Files.createFile(PATH);
        for (Group group1:groups){
            addGroupwhitId(group1);
        }
    }

    @Override
    public Group getGroupById(int id) throws IOException {
        Group group = null;
        for (Group group1 : getAllGroups()) {
            if (group1.getId() == id) {
                group = group1;
            }
        }
        return group;
    }
}
