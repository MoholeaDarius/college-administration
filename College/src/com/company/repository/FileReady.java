package com.company.repository;

import com.company.model.*;

import static com.company.repository.Constants.SEPARATOR1;
import static com.company.repository.Constants.SEPARATOR2;

public class FileReady {

    public static String student(Student student) {
        return student.getId() + SEPARATOR1 + student.getName() + SEPARATOR1 + student.getSurname() + SEPARATOR1 + student.getAge() + SEPARATOR1 + student.getGroupId();
    }

    public static String teacher(Teacher teacher) {
        return teacher.getId() + SEPARATOR1 + teacher.getName() + SEPARATOR1 + teacher.getSurname() + SEPARATOR1 + teacher.getAge() + SEPARATOR1 + teacher.getSubject();
    }

    public static String auxiliaryEmployee(AuxiliaryEmployee auxiliaryEmployee) {
        return auxiliaryEmployee.getId() + SEPARATOR1 + auxiliaryEmployee.getName() + SEPARATOR1 + auxiliaryEmployee.getSurname() + SEPARATOR1 + auxiliaryEmployee.getAge() + SEPARATOR1 + auxiliaryEmployee.getDuty();
    }

    public static String classroom(Classroom classroom) {
        return classroom.getId() + SEPARATOR1 + classroom.getNumber()+SEPARATOR1+classroom.getNumber();
    }

    public static String group(Group group) {
        return group.getId() + SEPARATOR1 + group.getName();
    }

    public static String college(College college) {
        return college.getName()+SEPARATOR2+college.getDirector().getName()+SEPARATOR1+college.getDirector().getSurname()+SEPARATOR1+college.getDirector().getAge()+SEPARATOR1+SEPARATOR2+college.getLocation().getStreetName()+SEPARATOR1+college.getLocation().getNumber();
    }

}
