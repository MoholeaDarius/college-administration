package com.company.repository;

import com.company.model.Classroom;

import java.io.IOException;
import java.util.List;

public interface ClassroomRepository {

    List<Classroom> getAllClassrooms() throws IOException;

    void addClassroom(Classroom newClassroom) throws IOException;

    void removeClassroom(int id) throws IOException;

    Classroom getClassroomById(int id) throws IOException;

}
