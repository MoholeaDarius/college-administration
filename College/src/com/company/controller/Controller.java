package com.company.controller;

import com.company.model.*;
import com.company.repository.*;

import javax.swing.table.JTableHeader;
import java.io.IOException;
import java.util.List;

public class Controller {

    private final CollegeRepository collegeRepository;
    private final AuxiliaryEmployeeRepository auxiliaryEmployeeRepository;
    private final StudentRepository studentRepository;
    private final ClassroomRepository classroomRepository;
    private final GroupRepository groupRepository;
    private final TeacherRepository teacherRepository;

    public Controller(CollegeRepository collegeRepository, AuxiliaryEmployeeRepository auxiliaryEmployeeRepository, StudentRepository studentRepository, ClassroomRepository classroomRepository, GroupRepository groupRepository, TeacherRepository teacherRepository) {
        this.collegeRepository = collegeRepository;
        this.auxiliaryEmployeeRepository = auxiliaryEmployeeRepository;
        this.studentRepository = studentRepository;
        this.classroomRepository = classroomRepository;
        this.groupRepository = groupRepository;
        this.teacherRepository = teacherRepository;
    }

    public College getCollege() throws IOException {
        return collegeRepository.getCollege();
    }

    public void changeDirector(Director newDirector) throws IOException {
        collegeRepository.changeDirector(newDirector);
    }

    public List<AuxiliaryEmployee> getAllAuxiliaryEmployee() throws IOException {
        return auxiliaryEmployeeRepository.getAuxiliaryEmployees();
    }

    public void removeAuxiliaryEmployee(int id) throws IOException {
        auxiliaryEmployeeRepository.removeAuxiliaryEmployee(id);
    }

    public void addAuxiliaryEmployee(AuxiliaryEmployee auxiliaryEmployee) throws IOException {
        auxiliaryEmployeeRepository.addAuxiliaryEmployee(auxiliaryEmployee);
    }

    public List<Classroom> getAllClassrooms() throws IOException {
        return classroomRepository.getAllClassrooms();
    }

    public void addClassroom(Classroom newClassroom) throws IOException {
        classroomRepository.addClassroom(newClassroom);
    }

    public void removeClassroom(int id) throws IOException {
        classroomRepository.removeClassroom(id);
    }

    public List<Group> getAllGroups() throws IOException {
        return groupRepository.getAllGroups();
    }

    public void addGroup(Group newGroup) throws IOException {
        groupRepository.addGroup(newGroup);
    }

    public void removeGroup(int id) throws IOException {
        groupRepository.removeGroup(id);
    }

    public List<Student> getAllStudents() throws IOException {
        return studentRepository.getAllStudents();
    }

    public void addStudent(Student newStudent) throws IOException {
        studentRepository.addStudent(newStudent);
    }

    public void removeStudent(int id) throws IOException {
        studentRepository.removeStudent(id);
    }

    public List<Teacher> getAllTeachers() throws IOException {
        return teacherRepository.getAllTeachers();
    }

    public void addTeacher(Teacher newTeacher) throws IOException {
        teacherRepository.addTeacher(newTeacher);
    }

    public void removeTeacher(int id) throws IOException {
        teacherRepository.removeTeacher(id);
    }

    public void changeLocation(Location newLocation) throws IOException {
        collegeRepository.changeLocation(newLocation);
    }

    public void changeName(String newName) throws IOException {
        collegeRepository.changeName(newName);
    }


}