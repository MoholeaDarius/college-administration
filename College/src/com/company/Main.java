package com.company;

import com.company.controller.Controller;
import com.company.repository.*;
import com.company.view.View;

import java.io.IOException;

import static com.company.Tools.Read.readInt;


public class Main {

    public static void main(String[] args) throws IOException {

        CollegeRepository collegeRepository = null;
        AuxiliaryEmployeeRepository auxiliaryEmployeeRepository = null;
        ClassroomRepository classroomRepository = null;
        GroupRepository groupRepository = null;
        StudentRepository studentRepository = null;
        TeacherRepository teacherRepository = null;
        System.out.print("Ce fel de memorie doriti sa folositi: \n" +
                "1. In memory.\n" +
                "2. In file.\n" +
                "Option: ");
        int option = readInt();
        if (option == 1) {
            collegeRepository = new InMemoryCollegeRepository();
            auxiliaryEmployeeRepository = new InMemoryAuxiliaryEmployeeRepository();
            classroomRepository = new InMemoryClassroomRepository();
            groupRepository = new InMemoryGroupRepository();
            studentRepository = new InFileStudentRepository();
            teacherRepository = new InMemoryTeacherRepository();
        } else if (option == 2) {
            studentRepository = new InFileStudentRepository();
            collegeRepository = new InFileCollegeRepository();
            auxiliaryEmployeeRepository = new InFileAuxiliaryEmployeeRepository();
            classroomRepository = new InFileClassroomRepository();
            groupRepository = new InFileGroupRepository();
            teacherRepository = new InFileTeacherRepository();
        }
        Controller controller = new Controller(collegeRepository, auxiliaryEmployeeRepository, studentRepository, classroomRepository, groupRepository, teacherRepository);
        View view = new View(controller);
        view.run();
    }

}
