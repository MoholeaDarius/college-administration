package com.company.view;

import com.company.controller.Controller;
import com.company.model.*;


import java.io.IOException;

import static com.company.Tools.Read.*;


public class View {

    private final Controller controller;

    public View(Controller controller) {
        this.controller = controller;
    }

    public void run() throws IOException{
        while (true) {
            printMenu();
            int option = readInt();
            if (option == 1) {
                printMenuLists();
                option = readInt();
                printLists(option);
                System.out.println();
            } else if (option == 2) {
                printAddMenu();
                option = readInt();
                addObjects(option);
                System.out.println();
            } else if (option == 3) {
                printRemoveMenu();
                option = readInt();
                removeObject(option);
                System.out.println();
            } else if (option == 4) {
                printChangeMenu();
                option = readInt();
                changeCollege(option);
                System.out.println();
            } else if (option == 0) {
                System.out.println("Exit.");
                break;
            }
        }
    }

    private void printMenu() throws IOException{
        System.out.print("\n------- " + controller.getCollege().getName() + " -------\n" +
                "1. Print.\n" +
                "2. Add.\n" +
                "3. Remove.\n" +
                "4. Change.\n" +
                "0. Exit.\n" +
                "Option: ");
    }

    private void printMenuLists() {
        System.out.print("1. Director.\n" +
                "2. Location.\n" +
                "3. Students.\n" +
                "4. Teachers.\n" +
                "5. Groups.\n" +
                "6. Classrooms.\n" +
                "7. Auxiliary employee.\n" +
                "0. Back.\n" +
                "Option: ");
    }

    private void printLists(int option)throws IOException {
        if (option == 1) {
            System.out.println(controller.getCollege().getDirector());
        }
        if (option == 2) {
            System.out.println(controller.getCollege().getLocation());
        }
        if (option == 3) {
            for (Student student : controller.getAllStudents()) {
                System.out.println(student);
            }
        }
        if (option == 4) {
            for (Teacher teacher : controller.getAllTeachers()) {
                System.out.println(teacher);
            }
        }
        if (option == 5) {
            for (Group group : controller.getAllGroups()) {
                System.out.println(group);
            }
        }
        if (option == 6) {
            for (Classroom classroom : controller.getAllClassrooms()) {
                System.out.println(classroom);
            }
        }
        if (option == 7) {
            for (AuxiliaryEmployee auxiliaryEmployee : controller.getAllAuxiliaryEmployee()) {
                System.out.println(auxiliaryEmployee);
            }
        }
        System.out.println();
    }

    private void printAddMenu() {
        System.out.print("1. Student.\n" +
                "2. Teacher.\n" +
                "3. Group.\n" +
                "4. Classroom.\n" +
                "5. Auxiliary employee.\n" +
                "0. Back.\n" +
                "Option: ");

    }

    private void addObjects(int option) throws IOException{
        String name;
        String surname;
        String subject;
        String duty;
        int age;
        int number;
        if (option == 1) {
            System.out.print("Introduceti numele studentului: ");
            name = readString();
            System.out.print("Introduceti prenumele studentului: ");
            surname = readString();
            System.out.print("Introduceti varsta studentului: ");
            age = readInt();
            Student newStudent = new Student(name, surname, age);
            controller.addStudent(newStudent);
        }
        if (option == 2) {
            System.out.print("Introduceti numele profesorului: ");
            name = readString();
            System.out.print("Introduceti prenumele profesorului: ");
            surname = readString();
            System.out.print("Introduceti varsta profesorului: ");
            age = readInt();
            System.out.print("Introduceti materia profesorului: ");
            subject = readString();
            Teacher newTeacher = new Teacher(name, surname, age, subject);
            controller.addTeacher(newTeacher);
        }
        if (option == 3) {
            System.out.print("Introduceti numele grupului: ");
            name = readString();
            Group newGroup = new Group(name);
            controller.addGroup(newGroup);
        }
        if (option == 4) {
            System.out.print("Introduceti numarul clasei: ");
            number = readInt();
            System.out.print("Introduceti tipul clasei: ");
            subject = readString();
            Classroom newClassroom = new Classroom(subject, number);
            controller.addClassroom(newClassroom);
        }
        if (option == 5) {
            System.out.print("Introduceti numele angajatului auxiliar: ");
            name = readString();
            System.out.print("Introduceti prenumele angajatului auxiliar: ");
            surname = readString();
            System.out.print("Introduceti varsta angajatului auxiliar: ");
            age = readInt();
            System.out.print("Introduceti responsabilitatea angajatului auxiliar: ");
            duty = readString();
            AuxiliaryEmployee newAuxiliaryEmployee = new AuxiliaryEmployee(name, surname, age, duty);
            controller.addAuxiliaryEmployee(newAuxiliaryEmployee);
        }
    }

    private void printRemoveMenu() {
        System.out.print("1. Student.\n" +
                "2. Teacher.\n" +
                "3. Group.\n" +
                "4. Classroom.\n" +
                "5. Auxiliary employee.\n" +
                "0. Back.\n" +
                "Option: ");
    }

    private void removeObject(int option)throws IOException {
        int id;
        printLists(option + 2);
        if (option == 1) {
            System.out.print("Introduceti id-ul studentului pe care doriti sa il stergeti: ");
            id = readInt();
            controller.removeStudent(id);
        }
        if (option == 2) {
            System.out.print("Introduceti id-ul profesorului pe care doriti sa il stergeti: ");
            id = readInt();
            controller.removeTeacher(id);
        }
        if (option == 3) {
            System.out.print("Introduceti id-ul grupului pe care doriti sa il stergeti: ");
            id = readInt();
            controller.removeGroup(id);
        }
        if (option == 4) {
            System.out.print("Introduceti id-ul clasei pe care doriti sa o stergeti: ");
            id = readInt();
            controller.removeClassroom(id);
        }
        if (option == 5) {
            System.out.print("Introduceti id-ul angajatului auxiliar pe care doriti sa il stergeti: ");
            id = readInt();
            controller.removeAuxiliaryEmployee(id);
        }
    }

    private void printChangeMenu() {
        System.out.print("1. Schimbati numele facultati.\n" +
                "2. Schimbati adresa facultati.\n" +
                "3. Schimbati directorul facultati.\n" +
                "0. Inapoi.\n" +
                "Option: ");
    }

    private void changeCollege(int option)throws IOException {
        String name;
        String surname;
        String streetName;
        int number;
        int age;
        if (option == 1) {
            System.out.print("Introduceti noul nume al facultati: ");
            name = readString();
            controller.changeName(name);
        }
        if (option == 2) {
            System.out.print("Introduceti noua strada: ");
            streetName = readString();
            System.out.print("Introduceti numarul");
            number = readInt();
            Location newLocation = new Location(streetName, number);
            controller.changeLocation(newLocation);
        }
        if (option == 3) {
            System.out.print("Introduceti numele noului director: ");
            name = readString();
            System.out.print("Introduceti prenumele noului director: ");
            surname = readString();
            System.out.print("Introduceti varsta moului director: ");
            age = readInt();
            Director newDirector = new Director(name, surname, age);
            controller.changeDirector(newDirector);
        }

    }

}

