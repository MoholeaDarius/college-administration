package com.company.model;

import java.util.Objects;

public class Classroom {

    private int id;
    private String subject;
    private int number;


    public Classroom(int id, String subject, int number) {
        this.id = id;
        this.subject = subject;
        this.number = number;
    }

    public Classroom(String subject, int number) {
        this.subject = subject;
        this.number = number;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Classroom that = (Classroom) o;
        return id == that.id && number == that.number && Objects.equals(subject, that.subject);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, subject, number);
    }

    @Override
    public String toString() {
        return "Classrooms{" +
                "id=" + id +
                ", subject='" + subject + '\'' +
                ", number=" + number +
                '}';
    }

}
