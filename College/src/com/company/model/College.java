package com.company.model;

import java.util.List;

public class College {

    private String name;
    private Location location;
    private Director director;
//    private List<Student> students;
//    private List<Teacher> teachers;
//    private List<Classroom> classroom;
//    private List<AuxiliaryEmployee> auxiliaryEmployees;


    public College(String name, Location location, Director director) {
        this.name = name;
        this.location = location;
        this.director = director;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

//    public List<Student> getStudents() {
//        return students;
//    }
//
//    public void setStudents(List<Student> students) {
//        this.students = students;
//    }
//
//    public List<Teacher> getTeachers() {
//        return teachers;
//    }
//
//    public void setTeachers(List<Teacher> teachers) {
//        this.teachers = teachers;
//    }
//
//    public List<Classroom> getClassroom() {
//        return classroom;
//    }
//
//    public void setClassroom(List<Classroom> classroom) {
//        this.classroom = classroom;
//    }
//
//    public List<AuxiliaryEmployee> getAuxiliaryEmployees() {
//        return auxiliaryEmployees;
//    }
//
//    public void setAuxiliaryEmployees(List<AuxiliaryEmployee> auxiliaryEmployees) {
//        this.auxiliaryEmployees = auxiliaryEmployees;
//    }

}
