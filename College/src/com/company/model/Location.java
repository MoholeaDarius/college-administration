package com.company.model;

import java.util.Objects;

public class Location {

    private String streetName;
    private int number;

    public Location(String streetName, int number) {
        this.streetName = streetName;
        this.number = number;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Location{" +
                "streetName='" + streetName + '\'' +
                ", number=" + number +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return number == location.number && Objects.equals(streetName, location.streetName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(streetName, number);
    }

}
