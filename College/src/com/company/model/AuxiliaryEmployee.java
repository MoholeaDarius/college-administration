package com.company.model;

import java.util.Objects;

public class AuxiliaryEmployee {

    private int id;
    private String name;
    private String surname;
    private int age;
    private String duty;

    public AuxiliaryEmployee(int id, String name, String surname, int age, String duty) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.duty = duty;
    }

    public AuxiliaryEmployee(String name, String surname, int age, String duty) {
        this.name = name;
        this.surname = surname;
        this.age = age;
        this.duty = duty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getDuty() {
        return duty;
    }

    public void setDuty(String duty) {
        this.duty = duty;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuxiliaryEmployee that = (AuxiliaryEmployee) o;
        return id == that.id && age == that.age && Objects.equals(name, that.name) && Objects.equals(surname, that.surname) && Objects.equals(duty, that.duty);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, age, duty);
    }

    @Override
    public String toString() {
        return "AuxiliaryEmployees{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", duty='" + duty + '\'' +
                '}';
    }

}
