package com.company.Tools;

import java.util.Scanner;

public class Read {

    static Scanner scan = new Scanner(System.in);
    static Scanner scanString = new Scanner(System.in);

    public static int readInt() {
        return scan.nextInt();
    }

    public static String readString() {
        return scanString.nextLine();
    }

}
